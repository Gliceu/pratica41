
package utfpr.ct.dainf.if62c.pratica;

public class Circulo extends Elipse {

      public double raio;

    public Circulo(double raio, double r, double s) {//super construtor
        super(r, s);
        this.raio = raio;
    }
     //public Circulo(){}

    public Circulo(double raio) {// contrutor que receber o valor do raio como argumento
        this.raio = raio;
    }
     
      @Override
    public double getArea() { //método para calcular a área do ciruclo
        double areaCirculo;
        areaCirculo = Math.PI * raio * raio;
        return areaCirculo;
    }

      @Override
    public double getPerimetro() {//metodo para calcular o perímetro do ciruclo
        double periCirculo;
        periCirculo = 2 * Math.PI * raio;
        return periCirculo;
    }
    
}

