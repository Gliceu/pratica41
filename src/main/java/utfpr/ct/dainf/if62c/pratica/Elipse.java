
package utfpr.ct.dainf.if62c.pratica;


public class Elipse  {

    public double r;
    public double s;

    public Elipse(double r, double s) {//construtor
        this.r = r;
        this.s = s;
    }
    public Elipse(){}

 
    public double getArea() {//método que calcula a área da elipse
        double areaElipse;
        areaElipse = Math.PI * r * s;
        return areaElipse;
    }

    public double getPerimetro() {//metodo que calcula o perímetro da elipse.
        double periElipse;
        periElipse = Math.PI * (3 * (r + s) - Math.sqrt((3 * r + s) * (r + 3 * s)));
        return periElipse;
    }
}