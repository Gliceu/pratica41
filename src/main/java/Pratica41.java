
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Gliceu Fernandes de Camargo <gliceu@utfpr.edu.br>
 */
public class Pratica41 {
    public static void main(String[] args) {
        Elipse elipse = new Elipse(10,10);
        Circulo circulo = new Circulo(10);

        System.out.printf("%.2f\n",elipse.getArea());
         System.out.printf("%.2f\n",elipse.getPerimetro());
          System.out.printf("%.2f\n",circulo.getArea());
           System.out.printf("%.2f\n",circulo.getPerimetro());
    }
}

